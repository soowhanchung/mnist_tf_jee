import tensorflow as tf
import numpy as np

print('soowhan helloworld')

# define simulation settings
max_epoch = 10
in_dim = 28 * 28
out_dim = 10

# define model settings

# load input/output data
fid = open('t10k-images-idx3-ubyte', 'rb')
fid.seek(16, 1)
test_image = np.frombuffer(fid.read(), np.dtype('uint8')) / 255
fid.close()
test_image.shape = (test_image.size // in_dim, in_dim)

fid = open('t10k-labels-idx1-ubyte', 'rb')
fid.seek(8, 1)
test_label = np.frombuffer(fid.read(), np.dtype('uint8'))
fid.close()

fid = open('train-images-idx3-ubyte', 'rb')
fid.seek(16, 1)
train_image = np.frombuffer(fid.read(), np.dtype('uint8')) / 255
fid.close()
train_image.shape = (train_image.size // in_dim, in_dim)

fid = open('train-labels-idx1-ubyte', 'rb')
fid.seek(8, 1)
train_label = np.frombuffer(fid.read(), np.dtype('uint8'))
fid.close()

print(test_image.shape)
print(train_image.shape)
print(test_label.shape)
print(train_label.shape)

# generate one-hot label
test_onehot = np.zeros([10000, out_dim])
train_onehot = np.zeros([60000, out_dim])
for idx in range(10000):
  test_onehot[idx, test_label[idx]] = 1
for idx in range(60000):
  train_onehot[idx, train_label[idx]] = 1


# initialize input and output placeholder
in_data = tf.placeholder(tf.float32, [None, in_dim])
out_data = tf.placeholder(tf.float32, [None, out_dim])

# generate model
w1 = tf.Variable(tf.random_normal([in_dim, 256], stddev=1))
b1 = tf.Variable(tf.random_normal([256], stddev=1))

w2 = tf.Variable(tf.random_normal([256, 256], stddev=1))
b2 = tf.Variable(tf.random_normal([256], stddev=1))

w3 = tf.Variable(tf.random_normal([256, out_dim], stddev=1))
b3 = tf.Variable(tf.random_normal([out_dim], stddev=1))

h1 = tf.tanh(tf.add(tf.matmul(in_data, w1), b1))
h2 = tf.tanh(tf.add(tf.matmul(h1, w2), b2))
out_prob = tf.add(tf.matmul(h2, w3), b3)

# set cost function and learning module
xent = tf.nn.softmax_cross_entropy_with_logits(labels=out_data, logits=out_prob)
cost = tf.reduce_mean(xent)
opt_op = tf.train.AdagradOptimizer(0.01).minimize(cost)
pred = tf.equal(tf.argmax(out_data, 1), tf.argmax(out_prob, 1))
acc = tf.reduce_mean(tf.cast(pred, tf.float32))

# initialize session and variables
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

accuracy = np.zeros([max_epoch + 1, 3])

# evaluate initial model
print(' Epoch %5d / %5d: ' % (0, max_epoch), flush=True, end='')
tr_acc = sess.run(acc, feed_dict={in_data: train_image[: 50000],
                                  out_data: train_onehot[: 50000]})
dv_acc = sess.run(acc, feed_dict={in_data: train_image[50000 :],
                                  out_data: train_onehot[50000 :]})
ts_acc = sess.run(acc, feed_dict={in_data: test_image, out_data: test_onehot})
print('%8.4f %8.4f %8.4f' % (100 * tr_acc, 100 * dv_acc, 100 * ts_acc))
accuracy[0] = [tr_acc, dv_acc, ts_acc]

for epoch in range(max_epoch):
  print(' Epoch %5d / %5d: ' % (epoch + 1, max_epoch), flush=True, end='')

  # minibatch sequence generator
  minibatch_idx = np.random.permutation(50000)
  minibatch_idx.shape = (500, 100)

  # run minibatch training
  for minibatch in minibatch_idx:
    x = train_image[minibatch]
    y = train_onehot[minibatch]
    sess.run(opt_op, feed_dict={in_data: x, out_data: y})

  # evaluate model
  tr_acc = sess.run(acc, feed_dict={in_data: train_image[0 : 50000],
                                    out_data: train_onehot[0 : 50000]})
  dv_acc = sess.run(acc, feed_dict={in_data: train_image[50000 :],
                                    out_data: train_onehot[50000 :]})
  ts_acc = sess.run(acc, feed_dict={in_data: test_image, out_data: test_onehot})

  # display and save results
  print('%8.4f %8.4f %8.4f' % (100 * tr_acc, 100 * dv_acc, 100 * ts_acc))
  accuracy[epoch + 1] = [tr_acc, dv_acc, ts_acc]

print(accuracy * 100)
sess.close()
